package com.aras.modir.okhttp;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aras.modir.okhttp.Models.IMDBPojo;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.orhanobut.hawk.Hawk;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText word;
    TextView title;
    TextView director;
    ImageView poster;
    Button search;

    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Hawk.init(this).build();


        dialog = new ProgressDialog(this);
        dialog.setTitle("Waiting");
        dialog.setMessage("Please Wait for Server Response");
        bind();
    }

    private void bind() {
        word = findViewById(R.id.word);
        title = findViewById(R.id.title);
        director = findViewById(R.id.director);
        poster = findViewById(R.id.poster);
        search = findViewById(R.id.button_search);
        search.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_search) {
            searchIMDB(word.getText().toString());
        }
    }

    private void searchIMDB(String word) {
        final String URL = "http://www.omdbapi.com/?t=" + word + "&apikey=70ad462a";
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(URL, new TextHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                dialog.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(MainActivity.this, "Error: " + throwable.toString()
                        , Toast.LENGTH_SHORT).show();
                String offlineSaved = Hawk.get(URL);
                if (offlineSaved != null) {
                    parseServerResponse(offlineSaved);
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
//                parseServerResponse(responseString);
                parseByGson(responseString);
                Hawk.put(URL, responseString);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                dialog.dismiss();
            }
        });
    }

    void parseServerResponse(String serverResponse) {

        try {

            JSONObject allObject = new JSONObject(serverResponse);

//            String ratingStr = allObject.getString("Ratings");
//            JSONObject ratingObj = new JSONObject(ratingStr);
//            String temp = ratingObj.getString("temp");


            String titleValue = allObject.getString("Title");
            String directorValue = allObject.getString("Director");
            String posterValue = allObject.getString("Poster");

            title.setText(titleValue);
            director.setText(directorValue);
            Picasso.get().load(posterValue).into(poster);

        } catch (JSONException e) {
            Toast.makeText(this, "Error: " + e.toString(), Toast.LENGTH_SHORT).show();
        }
    }


    void parseByGson(String serverResponse) {
        Gson gson = new Gson();
        IMDBPojo pojo = gson.fromJson(serverResponse, IMDBPojo.class);
        String directorOne = pojo.getDirector();
        String titleOne = pojo.getTitle();
        String posterOne = pojo.getPoster();

        title.setText(titleOne);
        director.setText(directorOne);
        Picasso.get().load(posterOne).into(poster);

    }

}
