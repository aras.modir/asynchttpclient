package com.aras.modir.okhttp;

import android.app.Application;
import android.content.Context;

import com.orhanobut.hawk.Hawk;

public class MyApplication extends Application {
    public Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        Hawk.init(mContext).build();
    }
}
